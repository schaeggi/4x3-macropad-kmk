# 12 key macropad using kmk

![Macripad Foto](/extras/pic/side.jpg)

## Used Hardware

- Adafruit QT Py RP2040
- Adafruit NeoKey 5x6 Ortho Snap-Apart

## Software Requirements

- Circuit Python v7.2.5 (https://circuitpython.org/downloads)
- KMK (https://github.com/KMKfw/kmk_firmware)
- neopixel.mpy library (https://github.com/adafruit/Adafruit_CircuitPython_Bundle/releases/tag/20221220)

## How to get it working

Copy the kmk folder to CIRCUITPY and neopixel.mpy to lib/
Check the used pins defined in lib/constants.py

## DevMode

By default, usb storage and repl are disabled. To enter DevMode plug in the macropad and press the top left button ("7") until all leds turn white. Now it will mount CIRCUITPY and REPL will be enabled.
  
## Default layer configuration
  
Set up for XFCE + custom shortcuts
  

### Layer 0

| L0  |     |     |     |
| --- | --- | --- | --- |
| MM  | SO  | TT  | S2  |
| PP  | V-  | V+  | T2  |
| D1  | D2  | D3  | T3  |

MM -> mute microphone (toggle)  
SO -> steam overlay  
TT -> tilix terminal quakemode  
S2 -> switch to layer 2  
PP -> play/pause media  
V- -> volume down  
V+ -> volume up  
T2 -> toggle layer 2  
Dn -> Go to desktop n (1-3)  
T3 -> toggle layer 3  
  
  
### Layer 1 (NUM)

| L1  |     |     |     |
| --- | --- | --- | --- |
| N7  | N8  | N9  | S1  |
| N4  | N5  | N6  | N0  |
| N1  | N2  | N3  | NE  |

Nn -> numpad key n  
NE -> numpad enter key  
S1 -> switch to layer 1  
  

### Layer 2 (TILING)

| L2  |     |     |     |
| --- | --- | --- | --- |
| TL  | TC  | TR  |     |
| ML  | KT  | MK  | --- |
| BL  | BC  | BR  |     |

TL -> Put window in the top left corner  
TC -> Put window in the top half of the screen  
TR -> Put window in the top right corner  
ML -> Put window in the left half of the screen  
KT -> Keep window on top (toggle)  
MR -> Put window in the right half of the screen  
BL -> Put window in the bottom left corner  
BC -> Put window in the bottom half of the screen  
BR -> Put window in the bottom right corner  
  
    
### Layer 3 (DESKTOPS)
  
| L3  |     |     |     |
| --- | --- | --- | --- |
|     |     |     | TL  |
| ML  | MR  | AD  |     |
| M1  | M2  | M3  | --- |
  
TL -> Toggle leds on layer 0  
ML -> Move window to the previous desktop  
MR -> Move window to the next desktop  
AD -> keep active window on all desktops  
Mn -> move window to desktop Dn (1-3)  
  