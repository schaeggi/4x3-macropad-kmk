import board
import supervisor
import digitalio
import usb_midi

""" Enable devmode on keypress when booting 
press top left switch while booting and release when all keys turn white """

# DiodeOrientation.ROW2COL
STORAGE_PIN_ROW = board.D4
STORAGE_PIN_COL = board.D3

print("board booting")

source = digitalio.DigitalInOut(STORAGE_PIN_ROW)
source.switch_to_output(value=True, drive_mode=digitalio.DriveMode.PUSH_PULL)

devmode_button = digitalio.DigitalInOut(STORAGE_PIN_COL)
devmode_button.switch_to_input(pull=digitalio.Pull.DOWN)

usb_midi.disable()  # always


if devmode_button.value:  # devmode on
    print("... devmode on")
    import neopixel
    import time
    from lib.constants import RGB_PIN_QTPY as RGB_PIN
    from lib.constants import COLS, ROWS
    pixels = neopixel.NeoPixel(pin=RGB_PIN, n=ROWS*COLS, brightness=1)
    pixels.fill((255, 255, 255))
    time.sleep(1)
    pixels.fill((0, 0, 0))
    pixels.deinit()

else:  # devmode off
    print("... devmode off")
    import storage
    import usb_cdc
    storage.disable_usb_drive()
    usb_cdc.disable()

source.deinit()
devmode_button.deinit()

""" Enable devmode end """

supervisor.set_next_stack_limit(4096 + 4096)
