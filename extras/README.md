# Script to toggle microphone on/off

## Requirements

- alsa-utils

        sudo apt-get install alsa-utils

## Installation

run install script to copy the scrept to ~/.local/bin/

    sh ./install

Set a shortcut to run the command

    mute_microphone