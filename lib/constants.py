import board

COLS = 4
ROWS = 3

BOARD_RGB_QTPY = board.NEOPIXEL
RGB_PIN_QTPY = board.D10
ROW_PINS_QTPY = (board.D4, board.D5, board.D6)
COL_PINS_QTPY = (board.D3, board.D2, board.D1, board.D0)

COLOR = {
    "off": (0, 0 ,0),

    "white": (255, 255, 255),
    "white-dark": (128, 128, 128),

    "red-light": (255, 128, 128),
	"red": (255, 0, 0),
    "red-dark": (128, 0, 0),
	
    "orange": (255, 128, 0),
    
    "yellow-light": (255, 255, 128),
    "yellow": (255, 255, 0),
    "yellow-dark": (128, 128, 0),

    "grass": (128, 255, 0),

    "green-light": (128, 255, 128),
    "green": (0, 255, 0),
    "green-dark": (0, 128, 0),

    "ocean": (0, 255, 128),

    "cyan-light": (128, 255, 255),
    "cyan": (0, 255, 255),
    "cyan-dark": (0, 128, 128),

    "sky": (0, 128, 255),

    "blue-light": (128, 128, 255),
    "blue": (0, 0, 255),
    "blue-dark": (0, 0, 128),

    "purple": (128, 0, 255),

    "magenta-light": (255, 128, 255),
    "magenta": (255, 0, 255),
    "magenta-dark": (128, 0, 128),

    "pink": (255, 0, 128),
}
