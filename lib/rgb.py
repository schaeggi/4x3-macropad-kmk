
import neopixel
from lib.constants import RGB_PIN_QTPY as RGB_PIN
from lib.constants import BOARD_RGB_QTPY as BOARD_RGB
from lib.constants import COLS, ROWS, COLOR

class RgbMapping:

    def __init__(self):
        self.keys = neopixel.NeoPixel(pin=RGB_PIN, n=ROWS*COLS, brightness=0.1)
        self.board_pixel = neopixel.NeoPixel(pin=BOARD_RGB, n=1, brightness=1)  # Onboard RGB LED
        self.default_dark = False
    
    def _set_color(self, key, color):  # TODO: check data      
        if self.default_dark == False:
            if key == 0:
                k = 11
            elif key == 1:
                k = 10
            elif key == 2:
                k = 9
            elif key == 3:
                k = 8
            elif key == 8:
                k = 3
            elif key == 9:
                k = 2
            elif key == 10:
                k = 1
            elif key == 11:
                k = 0
            else:
                k = key
            self.keys[k] = color
        
        else:
            return False

    def set_color(self, key, color):  # TODO: check data
        if self.default_dark == False:
            self.keys[key] = color
        else:
            return False

    def turn_off(self):
        self.keys.fill(COLOR["off"])
        # self.default_dark = True


    def set_default_pixels(self, *args):
        if self.default_dark == False:
            # upper row left-to-right
            self.keys[11] = COLOR["green"]
            self.keys[10] = COLOR["yellow-dark"]
            self.keys[9] = COLOR["orange"]
            self.keys[8] = COLOR["off"]
            # middle row left-to-right
            self.keys[4] = COLOR["green"]
            self.keys[5] = COLOR["green-light"]
            self.keys[6] = COLOR["green-light"]
            self.keys[7] = COLOR["off"]
            # lower row left-to-right
            self.keys[3] = COLOR["cyan-light"]
            self.keys[2] = COLOR["cyan-light"]
            self.keys[1] = COLOR["cyan-light"]
            self.keys[0] = COLOR["off"]
        
        else:
            self.keys.fill(COLOR["off"])
     
    def set_numpad_pixels(self, *args):
        # upper row left-to-right
        self.keys[11] = (255, 255, 255)
        self.keys[10] = (255, 255, 255)
        self.keys[9] = (255, 255, 255)
        self.keys[8] = (255, 0, 0)
        # middle row left-to-right
        self.keys[4] = (255, 255, 255)
        self.keys[5] = (255, 255, 255)
        self.keys[6] = (255, 255, 255)
        self.keys[7] = (255, 255, 255)	
        # lower row left-to-right
        self.keys[3] = (255, 255, 255)
        self.keys[2] = (255, 255, 255)
        self.keys[1] = (255, 255, 255)
        self.keys[0] = (0, 255, 0)
        
    def set_window_pixels(self, *args):
        # upper row left-to-right
        self.keys[11] = (255, 255, 255)
        self.keys[10] = (255, 255, 255)
        self.keys[9] = (255, 255, 255)
        self.keys[8] = (0, 0, 0)
        # middle row left-to-right
        self.keys[4] = (255, 255, 255)
        self.keys[5] = (0, 255, 255)
        self.keys[6] = (255, 255, 255)
        self.keys[7] = (255, 0, 0)
        # lower row left-to-right
        self.keys[3] = (255, 255, 255)
        self.keys[2] = (255, 255, 255)
        self.keys[1] = (255, 255, 255)
        self.keys[0] = (0, 0, 0)

    def set_desktop_pixels(self, *args):
        # upper row left-to-right
        self.keys[11] = (0, 0, 0)
        self.keys[10] = (0, 0, 0)
        self.keys[9] = (0, 0, 0)
        self.keys[8] = (0, 0, 0)
        # middle row left-to-right
        self.keys[4] = (255, 255, 255)
        self.keys[5] = (255, 255, 255)
        self.keys[6] = (0, 255, 255)
        self.keys[7] = (0, 0, 0)
        # lower row left-to-right
        self.keys[3] = (255, 255, 255)
        self.keys[2] = (255, 255, 255)
        self.keys[1] = (255, 255, 255)
        self.keys[0] = (255, 0, 0)