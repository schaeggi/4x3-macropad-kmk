from kmk.kmk_keyboard import KMKKeyboard
from kmk.keys import KC
from kmk.extensions.media_keys import MediaKeys
from kmk.scanners import DiodeOrientation
from kmk.modules.layers import Layers

from lib.rgb import RgbMapping

from lib.constants import COLOR
from lib.constants import ROW_PINS_QTPY as ROW_PINS
from lib.constants import COL_PINS_QTPY as COL_PINS

keyboard = KMKKeyboard()
keyboard.modules.append(Layers())
keyboard.extensions.append(MediaKeys())


class layers:

    state_media = 0
    state_mic = 0

    TERMINAL_QUAKE = KC.LSHIFT(KC.LCTL(KC.LALT(KC.Q))) #  KC.LGUI(KC.N1)  # Toggle dropdown terminal
    TERMINAL = KC.LSHIFT(KC.LCTL(KC.LALT(KC.T))) #  KC.LGUI(KC.T)  # Open new terminal
    TASK_MANAGER = KC.LCTL(KC.LALT(KC.DEL))  # Open task manager
    MUTE_MICROPHONE = KC.LSHIFT(KC.LCTL(KC.LALT(KC.M)))  # KC.LSHIFT(KC.LGUI(KC.PAUSE))  # Mute microphone
    STEAM_OVERLAY = KC.LSHIFT(KC.LCTL(KC.LALT(KC.S)))  # KC.LSHIFT(KC.LGUI(KC.SCROLLLOCK))  # Steam overlay

    keys_default = (  # KEYS LAYER 0 / BASE LAYER
        # upper row left-to-right
        MUTE_MICROPHONE,  # KC.LSHIFT(KC.LGUI(KC.PAUSE)),  # Mute microphone
        STEAM_OVERLAY,  # KC.LSHIFT(KC.LGUI(KC.SCROLLLOCK)),  # Steam overlay (Shift, Win ScrollLock)
        TERMINAL_QUAKE,  # KC.SCROLLLOCK,  # Quake mode tilix
        KC.TO(1),  # Toggle layer1 (numpad)
        # middle row left-to-right
        KC.MEDIA_PLAY_PAUSE,
        KC.AUDIO_VOL_DOWN,
        KC.AUDIO_VOL_UP,
        KC.MO(2),
        # lower row left-to-right
        KC.LCTL(KC.F1),  # Switch to desktop1
        KC.LCTL(KC.F2),  # Switch to desktop2
        KC.LCTL(KC.F3),  # Switch to desktop3
        KC.MO(3), 	# Switch to layers.desktop
    )

    keys_numpad = (  # KEYS LAYER 1 / NUMPAD LAYER
        # upper row left-to-right
        KC.KP_7,
        KC.KP_8,
        KC.KP_9,
        KC.TO(0),
        # middle row left-to-right
        KC.KP_4,
        KC.KP_5,
        KC.KP_6,
        KC.KP_0,
        # lower row left-to-right
        KC.KP_1,
        KC.KP_2,
        KC.KP_3,
        KC.KP_ENTER,
    )

    keys_window = (  # KEYS LAYER 2 / WINDOW LAYER
        # upper row left-to-right
        KC.LGUI(KC.KP_7),
        KC.LGUI(KC.UP),
        KC.LGUI(KC.KP_9),
        KC.NO,  # ,  #
        # middle row left-to-right	
        KC.LGUI(KC.LEFT),
        KC.LALT(KC.F12),  # maximize window
        KC.LGUI(KC.RIGHT),
        KC.NO,  # KC.TO(0),
        # lower row left-to-right
        KC.LGUI(KC.KP_1),
        KC.LGUI(KC.DOWN),
        KC.LGUI(KC.KP_3),
        KC.NO,  # KC.LALT(KC.F12),  #
    )

    keys_desktop = (  # KEYS LAYER 3 / DESKTOP LAYER
        # upper row left-to-right
        KC.NO,
        KC.NO,
        KC.NO,
        KC.NO,
        # middle row left-to-right		
        KC.LCTL(KC.LALT(KC.HOME)),  # Ctrl + Alt + Home
        KC.LCTL(KC.LALT(KC.END)),  # Ctrl + Alt + End
        KC.LALT(KC.F8),  # stick window
        KC.NO,
        # lower row left-to-right
        KC.LCTL(KC.LALT(KC.KP_1)),  # Move to desktop 1
        KC.LCTL(KC.LALT(KC.KP_2)),  # Move to desktop 2
        KC.LCTL(KC.LALT(KC.KP_3)),  # Move to desktop 3
        KC.NO,
    )


colors = RgbMapping()

def toggle_mic(*args):
    # print(args[-2])
    print("toggle mic")
    if layers.state_mic == 0:
        colors.set_color(11, COLOR["red-dark"])
        layers.state_mic = 1
    else:
        colors.set_color(11, COLOR["green-dark"])
        layers.state_mic = 0


def toggle_media(*args):
    print("toggle media")
    if layers.state_media == 0:
        colors.set_color(4, COLOR["red-dark"])
        layers.state_media = 1
    else:
        colors.set_color(4, COLOR["green-dark"])
        layers.state_media = 0

def toggle_screen(*args):
    if args[-2] == 8:
        print("desktop 1")
        colors._set_color(8, COLOR["cyan-dark"])
        colors._set_color(9, COLOR["cyan-light"])
        colors._set_color(10, COLOR["cyan-light"])
    elif args[-2] == 9:
        print("desktop 2")
        colors._set_color(8, COLOR["cyan-light"])
        colors._set_color(9, COLOR["cyan-dark"])
        colors._set_color(10, COLOR["cyan-light"])
    elif args[-2] == 10:
        print("desktop 3")
        colors._set_color(8, COLOR["cyan-light"])
        colors._set_color(9, COLOR["cyan-light"])
        colors._set_color(10, COLOR["cyan-dark"])

def toggle_lighting(*args):
    if colors.default_dark == True:
        colors.default_dark = False
    else:
        colors.default_dark = True

layers.keys_default[3].after_press_handler(colors.set_numpad_pixels)
layers.keys_numpad[3].after_press_handler(colors.set_default_pixels)

layers.keys_default[7].after_press_handler(colors.set_window_pixels)
layers.keys_default[7].after_release_handler(colors.set_default_pixels)

layers.keys_default[11].after_press_handler(colors.set_desktop_pixels)
layers.keys_default[11].after_release_handler(colors.set_default_pixels)

layers.keys_default[0].after_press_handler(toggle_mic)
layers.keys_default[4].after_press_handler(toggle_media)

layers.keys_default[10].after_press_handler(toggle_screen)
layers.keys_default[9].after_press_handler(toggle_screen)
layers.keys_default[8].after_press_handler(toggle_screen)

layers.keys_desktop[3].after_press_handler(toggle_lighting)

colors.set_default_pixels()

keyboard.col_pins = (COL_PINS[0], COL_PINS[1], COL_PINS[2], COL_PINS[3])
keyboard.row_pins = (ROW_PINS[0], ROW_PINS[1], ROW_PINS[2])
keyboard.diode_orientation = DiodeOrientation.ROW2COL
keyboard.tap_time = 130
keyboard.keymap = [layers.keys_default, layers.keys_numpad, layers.keys_window, layers.keys_desktop]

if __name__ == '__main__':
    keyboard.go()
